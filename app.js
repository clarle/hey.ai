const fs      = require('fs');

const _       = require('lodash');
const restify = require('restify');
const builder = require('botbuilder');

const config = fs.existsSync('./config/index.js') ? require('./config') : {};

const dashbotApiMap = {
    facebook: process.env.DASHBOT_API_KEY_FACEBOOK
};
const dashbot = require('dashbot')(dashbotApiMap).microsoft;
dashbot.setFacebookToken(process.env.FACEBOOK_PAGE_TOKEN)

// Dialogs
const StartDialog           = require('./dialogs/start');
const ConversationDialog    = require('./dialogs/conversation');
const LocationDialog        = require('./dialogs/location');
const PoliticalIssuesDialog = require('./dialogs/politicalIssues');
const BeginMatchmakingDialog = require('./dialogs/beginMatchmaking');
const EndMatchmakingDialog  = require('./dialogs/endMatchmaking');
const EconomicIssuesDialog  = require('./dialogs/economicsIssues');
const SocialIssuesDialog    = require('./dialogs/socialIssues');
const ForeignIssuesDialog   = require('./dialogs/foreignIssues')
const skip                  = require('./dialogs/skip')

// Helpers
const t = require('./helpers/localization');

//=========================================================
// Bot Setup
//=========================================================

// Setup Restify server
const server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, () => {
    console.log('%s listening to %s', server.name, server.url);
});

// Create chat bot
const connector = new builder.ChatConnector({
    appId       : process.env.MICROSOFT_BOT_APP_ID       || config.MICROSOFT_BOT_APP_ID,
    appPassword : process.env.MICROSOFT_BOT_APP_PASSWORD || config.MICROSOFT_BOT_APP_PASSWORD
});

const bot = new builder.UniversalBot(connector, {
    localizerSettings: {
        defaultLocale: 'en'
    }
});
bot.use(dashbot);

// Serve a static web page
server.get(/.*/, restify.serveStatic({
    'directory': './landing',
    'default': 'index.html'
}));

server.post('/api/messages', connector.listen());

//=========================================================
// Bots Dialogs
//=========================================================

bot.dialog('/', [
    function (session) {
        const name = _.get(session, 'message.participants.from', 'there');
        session.send("initial_prompt", name);
        builder.Prompts.choice(session, "begin_prompt", [
            t(session, "start_choice")
        ]);
    },
    function (session, results) {
        const mainDialogs = {
            [t(session, "start_choice")]: "/start"
        };
        const mainChoice = _.get(results, 'response.entity');
        const nextDialog = mainDialogs[mainChoice];

        if (nextDialog) {
            session.beginDialog(nextDialog);
        } else {
            session.send("generic_error");
        }
    }
]);

bot.dialog('/start', StartDialog);
bot.dialog('/conversation', ConversationDialog);
bot.dialog('/location', LocationDialog);
bot.dialog('/politicalIssues', PoliticalIssuesDialog);
bot.dialog('/beginMatchmaking', BeginMatchmakingDialog(bot));
bot.dialog('/endMatchmaking', EndMatchmakingDialog(bot));
bot.dialog('/economicsIssues', EconomicIssuesDialog);
bot.dialog('/socialIssues', SocialIssuesDialog);
bot.dialog('/foreignIssues', ForeignIssuesDialog);
bot.dialog('/skip', skip)

bot.beginDialogAction('beginMatchmaking', '/beginMatchmaking')
bot.beginDialogAction('endChat', '/endMatchmaking');
bot.beginDialogAction('politicalIssues', '/politicalIssues');
bot.beginDialogAction('economicsIssues', '/economicsIssues');
bot.beginDialogAction('socialIssues', '/socialIssues');
bot.beginDialogAction('foreignIssues', '/foreignIssues');
bot.beginDialogAction('skip', '/skip')
