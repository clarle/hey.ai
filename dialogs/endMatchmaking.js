"use strict";
const _       = require('lodash');
const builder = require('botbuilder');


// Helpers
const matchmaker = require('../helpers/matchmaker');
const t = require('../helpers/localization');

module.exports = function (bot) {
    return [
        function (session) {
            const image = new builder.AnimationCard(session)
                .media([{
                    profile: "bye_elephant",
                    url: "http://i.giphy.com/XSQUAevZhaROM.gif"
                }]);
            const message = new builder.Message(session)
                .addAttachment(image);

            session.send(message);
            matchmaker.endConversation(bot, session, session.message.address);
            builder.Prompts.choice(session, "approval_rating", [
                t(session, "good_experience"),
                t(session, "bad_experience"),
                t(session, "abuse_experience")
            ]);
        },

    	function (session, results) {
            const response = _.get(results, 'response.entity');
            if (response === t(session, "good_experience")) {
                session.send("good_reply")
            }
            else if (response == t(session, "bad_experience")){
                session.send("bad_reply")
            }
            else if (response === t(session, "abuse_experience")) {
                session.send("abuse_reply");
            }
            session.sessionState.isWaiting = false;
            session.beginDialog('/');
        }
    ];
}