const matchmaker = require('../helpers/matchmaker');

module.exports = function(bot) {
    return [
        function (session) {
            if (session.sessionState.scale < 6 && !session.sessionState.isWaiting) {
                session.sessionState.isWaiting = true;
                matchmaker.addLiberalUser(bot, session, session.message.address);
            } else if (!session.sessionState.isWaiting) {
                session.sessionState.isWaiting = true;
                matchmaker.addConservativeUser(bot, session, session.message.address);
            } else {
                session.send("You're already waiting for a match.");
            }
        }
    ];
}
