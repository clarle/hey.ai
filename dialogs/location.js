"use strict";
const _       = require('lodash');
const builder = require('botbuilder');
const states  = require('us-state-codes');

// Helpers
const t = require('../helpers/localization');

module.exports = [
    function (session) {
        builder.Prompts.text(session, "state_response");
    },

    function (session, results) {
        const response = _.get(results, 'response');
        const stateName = states.getStateNameByStateCode(response);

        if (stateName) { session.send("Great, I love %s 😄", stateName)
        session.beginDialog('/skip'); }
        else {
            session.send("I'm sorry I didn't get that. Let's try again.")
            session.beginDialog('/location')
        }

    },

];