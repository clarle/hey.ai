"use strict";
const _       = require('lodash');
const builder = require('botbuilder');

const matchmaker = require('../helpers/matchmaker');

module.exports = new builder.IntentDialog()
    .matches(/((?!endChat).)*$/, function (session, args) {
        const message = new builder.Message()
            .address(matchmaker.getOtherAddress(session))
            .text(args.matched[0]);

        session.send(message);
    });