"use strict";
const _       = require('lodash');
const builder = require('botbuilder');

// Helpers
const t = require('../helpers/localization');

module.exports = [

	function(session){
		const image = new builder.AnimationCard(session)
            .media([{
                profile: "pack_airplane",
                url: "http://i.giphy.com/d31wQ7ajDuaSoIJa.gif"
            }]);
      const message = new builder.Message(session)
            .addAttachment(image);

        session.send(message);
        builder.Prompts.choice(session,"foreign_one", [
        	t(session, "yes_immi"),
        	t(session, "no_immi")
        ]);
    },

    function(session, results){
        if (_.get(results, 'response.entity') === t(session, "yes_immi")) {
            session.sessionState.scale--;
        } else {
            session.sessionState.scale++;
        }
        session.send("Great to know!");
         builder.Prompts.choice(session,"foreign_two", [
            t(session, "yes_active"),
            t(session, "no_active")
        ]);
    },

    function(session, results){
        if (_.get(results, 'response.entity') === t(session, "no_active")) {
            session.sessionState.scale--;
        } else {
            session.sessionState.scale++;
        }
        session.send("Alright!");
        builder.Prompts.choice(session,"foreign_three", [
            t(session, "yes_aid"),
            t(session, "no_aid")
        ]);
	},

    function(session, results){
        if (_.get(results, 'response.entity') === t(session, "yes_aid")) {
            session.sessionState.scale--;
        } else {
            session.sessionState.scale++;
        }
        session.send("Great answers! Let's go back and explore more issues.")
        const response = _.get(results, 'response.entity');
        session.beginDialog('/politicalIssues');
    }
];