"use strict";
const _       = require('lodash');
const builder = require('botbuilder');

// Helpers
const t = require('../helpers/localization');

// GLOBAL STATE, GET RID OF (EVENTUALLY)
let childVotes = {
    "only"     : 0,
    "oldest"   : 0,
    "youngest" : 0,
    "other"    : 0
};

function calculatePercentages(obj) {
    const totalVotes = _.values(obj).reduce((sum, curr) => sum + curr, 0);

    return Object.keys(obj).reduce((percentages, curr) => {
        percentages[curr] = (obj[curr] / totalVotes) * 100;
        return percentages;
    }, {});
}

module.exports = [
    function (session) {
        session.send("great_response");
        builder.Prompts.choice(session, "explanation_response", t(session, "super_choice"));
    },

    function (session) {
        const image = new builder.AnimationCard(session)
            .media([{
                profile: "high_five_fallback",
                url: "http://i.giphy.com/l0ErFafpUCQTQFMSk.gif"
            }]);

        const message = new builder.Message(session)
            .addAttachment(image);

        session.send(message);
        builder.Prompts.choice(session, "same_page_response", t(session, "do_this_choice"));
    },

    function (session) {
        builder.Prompts.choice(session, "awesome_response", [
            t(session, "child_only_choice"),
            t(session, "child_oldest_choice"),
            t(session, "child_youngest_choice"),
            t(session, "child_other_choice")
        ]);
    },

    function (session, results) {
        const image = new builder.AnimationCard(session)
            .media([{
                profile: "brady_bunch_fallback",
                url: "http://i.giphy.com/1G3f5g87GE21W.gif"
            }]);
        const message = new builder.Message(session)
            .addAttachment(image);
        const response = _.get(results, 'response.entity', t(session, "child_other_choice"));

        let percentages;
        let resultsData;
        if (response === t(session, "child_only_choice")) {
            childVotes.only++;
            percentages = calculatePercentages(childVotes);
            resultsData = {
                firstPercentage: percentages.only.toString(),
                firstString: t(session, "child_only_choice").toLowerCase(),
                secondPercentage: percentages.oldest.toString(),
                secondString: t(session, "child_oldest_choice").toLowerCase(),
                thirdPercentage: percentages.youngest.toString(),
                thirdString: t(session, "child_youngest_choice").toLowerCase(),
                fourthPercentage: percentages.other.toString(),
                fourthString: t(session, "child_other_choice").toLowerCase()
            };
        } else if (response === t(session, "child_oldest_choice")) {
            childVotes.oldest++;
            percentages = calculatePercentages(childVotes);
            resultsData = {
                firstPercentage: percentages.oldest.toString(),
                firstString: t(session, "child_oldest_choice").toLowerCase(),
                secondPercentage: percentages.only.toString(),
                secondString: t(session, "child_only_choice").toLowerCase(),
                thirdPercentage: percentages.youngest.toString(),
                thirdString: t(session, "child_youngest_choice").toLowerCase(),
                fourthPercentage: percentages.other.toString(),
                fourthString: t(session, "child_other_choice").toLowerCase()
            };
        } else if (response === t(session, "child_youngest_choice")) {
            childVotes.youngest++;
            percentages = calculatePercentages(childVotes);
            resultsData = {
                firstPercentage: percentages.youngest.toString(),
                firstString: t(session, "child_youngest_choice").toLowerCase(),
                secondPercentage: percentages.only.toString(),
                secondString: t(session, "child_only_choice").toLowerCase(),
                thirdPercentage: percentages.oldest.toString(),
                thirdString: t(session, "child_oldest_choice").toLowerCase(),
                fourthPercentage: percentages.other.toString(),
                fourthString: t(session, "child_other_choice").toLowerCase()
            };
        } else if (response === t(session, "child_other_choice")) {
            childVotes.other++;
            percentages = calculatePercentages(childVotes);
            resultsData = {
                firstPercentage: percentages.other.toString(),
                firstString: t(session, "child_other_choice").toLowerCase(),
                secondPercentage: percentages.only.toString(),
                secondString: t(session, "child_only_choice").toLowerCase(),
                thirdPercentage: percentages.oldest.toString(),
                thirdString: t(session, "child_oldest_choice").toLowerCase(),
                fourthPercentage: percentages.youngest.toString(),
                fourthString: t(session, "child_youngest_choice").toLowerCase()
            };
        }

        session.send(message);
        session.send("child_response", resultsData);
        builder.Prompts.choice(session, "political_response", [
            t(session, "sure_choice")
        ]);
    },

    function (session) {
        session.beginDialog('/location');
    }
];