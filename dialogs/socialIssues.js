"use strict";
const _       = require('lodash');
const builder = require('botbuilder');

// Helpers
const t = require('../helpers/localization');

module.exports = [

	function(session){
		const image = new builder.AnimationCard(session)
            .media([{
                profile: "love_food",
                url: "http://i.giphy.com/vt0YRz0lQ2cnK.gif"
            }]);
      const message = new builder.Message(session)
            .addAttachment(image);

        session.send(message);
        builder.Prompts.choice(session,"social_one", [
        	t(session, "yes_gay"),
        	t(session, "no_gay")
        ]);
    },

    function(session, results){
        if (_.get(results, 'response.entity') === t(session, "yes_gay")) {
            session.sessionState.scale--;
        } else {
            session.sessionState.scale++;
        }
        session.send("Great to know!");
        builder.Prompts.choice(session,"social_two", [
            t(session, "yes_abortion"),
            t(session, "no_abortion")
        ]);
    },

    function(session, results) {
        if (_.get(results, 'response.entity') === t(session, "yes_abortion")) {
            session.sessionState.scale--;
        } else {
            session.sessionState.scale++;
        }
        session.send("Alright!");
        builder.Prompts.choice(session,"social_three", [
            t(session, "yes_mj"),
            t(session, "no_mj")
        ]);
	},

    function(session, results){
        if (_.get(results, 'response.entity') === t(session, "yes_mj")) {
            session.sessionState.scale--;
        } else {
            session.sessionState.scale++;
        }
        session.send("Great answers! Let's go back and explore more issues.")
        const response = _.get(results, 'response.entity');
        session.beginDialog('/politicalIssues');
    }
];