"use strict";
const _       = require('lodash');
const builder = require('botbuilder');

module.exports = [

	function(session){
	session.send("Let's talk some politics.")
    const card = new builder.HeroCard(session)
            .title('Economic Issues')
            .subtitle('Come talk your $$$.')
            .text('Time to show off your Adam Smith and Karl Marx references.')
            .buttons([builder.CardAction.dialogAction(session, "economicsIssues", "Let's talk economics!", "Let's talk $$$!")])
            .images([
                builder.CardImage.create(session, "https://images.unsplash.com/reserve/TxbDzeAhRmCwa6DDrbOQ_kazan-big.jpg?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&s=d3c644e45b716d1203c93b46dd8dfecbt")
            ]);

      const card2 = new builder.HeroCard(session)
            .title('Social Issues')
            .subtitle('Come talk about other people')
            .text('Society got you up or down?')
            .buttons([builder.CardAction.dialogAction(session, "socialIssues", "Let's talk society!", "Let's talk people!")])
            .images([
                builder.CardImage.create(session, "https://s3.amazonaws.com/ooomf-com-files/ZLSw0SXxThSrkXRIiCdT_DSC_0345.jpg")
            ]);

      const card3 = new builder.HeroCard(session)
            .title('Foreign Policy Issues')
            .subtitle('Come talk about other countries')
            .text('Time to look beyond the borders.')
            .buttons([builder.CardAction.dialogAction(session, "foreignIssues", "Let's talk other countries!", "Let's talk countries!")])
            .images([
                builder.CardImage.create(session, "https://images.unsplash.com/photo-1422222948315-28aadb7a2cb8?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&s=6a9fb9b5f601e1f02bffe5374fa010cf")
            ]);

      const card4 = new builder.HeroCard(session)
            .title('Match with somebody else')
            .subtitle("Think I've learned enough about you? Get paired now!")
            .text('Time to talk to somebody new!')
            .buttons([builder.CardAction.dialogAction(session, "beginMatchmaking", "I'm done!", "Let's meet someone new!")])
            .images([
                builder.CardImage.create(session, "http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_528a8fb8a276d_1.JPG")
            ]);

        const message = new builder.Message(session)
            .attachmentLayout(builder.AttachmentLayout.carousel)
            .attachments([card, card2, card3, card4]);

        session.send(message);
    }

];