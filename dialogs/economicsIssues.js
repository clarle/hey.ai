"use strict";
const _       = require('lodash');
const builder = require('botbuilder');

// Helpers
const t = require('../helpers/localization');

module.exports = [

	function(session){
		const image = new builder.AnimationCard(session)
            .media([{
                profile: "two_and_a_half_men",
                url: "http://i.giphy.com/DXzcQ6To6aNXy.gif"
            }]);
      const message = new builder.Message(session)
            .addAttachment(image);

        session.send(message);
        builder.Prompts.choice(session,"economics_one", [
        	t(session, "more_government"),
        	t(session, "less_government")
        ]);
    },

    function(session, results) {
        if (_.get(results, 'response.entity') === t(session, "more_government")) {
            session.sessionState.scale--;
        } else {
            session.sessionState.scale++;
        }
        session.send("Great to know!");
        builder.Prompts.choice(session,"economics_two", [
            t(session, "more_tax"),
            t(session, "less_tax")
        ]);
    },

    function(session, results) {
        if (_.get(results, 'response.entity') === t(session, "more_tax")) {
            session.sessionState.scale--;
        } else {
            session.sessionState.scale++;
        }
        session.send("Alright!");
        builder.Prompts.choice(session,"economics_three", [
            t(session, "more_trade"),
            t(session, "less_trade")
        ]);
	},

    function(session, results){
        if (_.get(results, 'response.entity') === t(session, "less_trade")) {
            session.sessionState.scale--;
        } else {
            session.sessionState.scale++;
        }
        session.send("Great answers! Let's go back and explore more issues.")
        const response = _.get(results, 'response.entity');
        session.beginDialog('/politicalIssues');
    }
];