"use strict";
const _       = require('lodash');
const builder = require('botbuilder');

// Helpers
const t = require('../helpers/localization');

module.exports = [

	function (session) {
		builder.Prompts.choice(session, "orient", [
                t(session, "yes_orient_conservative"),
                t(session, "yes_orient_liberal"),
                t(session, "no_orient")
            ]);
	},

    function (session, results) {
        const response = _.get(results, 'response.entity');
        session.sessionState.scale = 5;
        if (response === t(session, "yes_orient_conservative")) {
            session.sessionState.scale = 10;
        	session.beginDialog('/beginMatchmaking');
        }
        else if (response == t(session, "yes_orient_liberal")){
            session.sessionState.scale = 0;
			session.beginDialog('/beginMatchmaking')
		}
        else {
        	session.beginDialog('/politicalIssues');
        }

    }
];