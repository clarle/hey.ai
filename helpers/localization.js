function text(session, string) {
    return session.localizer.gettext(session.preferredLocale(), string);
};

module.exports = text;
