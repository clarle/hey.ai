"use strict";
const _       = require('lodash');
const builder = require('botbuilder');

let liberals = [];
let conservatives = [];
let conversations = {};
let addresses = {};

module.exports = {
    addLiberalUser: function(bot, session, address) {
        liberals.push(address);
        session.send("We're matching you with the best person to talk to. Feel free to come back later - we'll let you know when we've found the perfect person!");
        this.checkForMatches(bot, session);
    },

    addConservativeUser: function(bot, session, address) {
        conservatives.push(address);
        session.send("We're matching you with the best person to talk to. Feel free to come back later - we'll let you know when we've found the perfect person!");
        this.checkForMatches(bot, session);
    },

    checkForMatches: function(bot, session) {
        let liberal;
        let conservative;
        if (liberals.length > 0 && conservatives.length > 0) {
            liberal = liberals.pop();
            conservative = conservatives.pop();

            addresses[liberal.user.id] = liberal;
            addresses[conservative.user.id] = conservative;

            conversations[liberal.user.id] = conservative.user.id;
            conversations[conservative.user.id] = liberal.user.id;

            session.sessionState.isWaiting = false;

            const liberalMessage = new builder.Message()
                .address(liberal)
                .text("We found a match! You two have at least one opinion where you two differ. Start chatting and try to figure out what it is! End the conversation at any time from the bottom left: ☰");

            const conservativeMessage = new builder.Message()
                .address(conservative)
                .text("We found a match! You two have at least one opinion where you two differ. Start chatting and try to figure out what it is! End the conversation at any time from the bottom left: ☰");

            session.send(liberalMessage);
            session.send(conservativeMessage);
            bot.beginDialog(liberal, '/conversation');
            bot.beginDialog(conservative, '/conversation');
        }
    },

    getOtherAddress: function(session) {
        const userId  = _.get(session, 'message.user.id');
        const otherId = conversations[userId];
        return addresses[otherId];
    },

    endConversation: function(bot, session, address) {
        const userId  = address.user.id;
        const otherId = conversations[userId];
        const userAddress  = addresses[userId];
        const otherAddress = addresses[otherId];

        if (userAddress) {
            const userMessage = new builder.Message()
                .address(userAddress)
                .text("The conversation has ended since one of you has left.");
            session.send(userMessage);
        }

        if (otherAddress) {
            const otherMessage = new builder.Message()
                .address(otherAddress)
                .text("The conversation has ended since one of you has left.");
            session.send(otherMessage);
            bot.beginDialog(otherAddress, '/');
        }

        _.remove(liberals, (address) => address.user.id === userId || address.user.id === otherId);
        _.remove(conservatives, (address) => address.user.id === userId || address.user.id === otherId);
        delete addresses[userId];
        delete addresses[otherId];
        delete conversations[userId];
        delete conversations[otherId];
    }
};